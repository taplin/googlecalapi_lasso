<?Lassoscript
	define_type('ae_googlesync');
		
		local('cal' = googlecal_api());
		local('event_data' = map());
		local('event_id');
		
		local('calendarID' = 'calendar ID which will be updated')
		
		define_tag('oncreate', -required='eventid');
			self->cal->g_auth->auth_tokens = ae_googletokens->fetch;
			self->cal->g_auth->check_tokens();
			self->event_id = #eventid;
			
			local('eventsql' = 'select evStartDate, evEndDate, evCityState, evTitle, googlecal_id, googlecal_eventid from onsiteEvents where id = "'+#eventid+'";');
			
			inline(-database="test", -table="onsiteEvents", -sql=#eventsql);
				self->event_data = records_map(-type=array)->first->second;
			/inline;
		
		/define_tag;
		
		define_tag('getCalendarID');
			//use cal_list to retrieve dynamic primaryID
			//local('cal_list' = self->cal->calendarlist_get());
			//return(#cal_list->primaryID);
			
			//use hard coded calendar id for Static Calendar
			return(self->calendarID);
		/define_tag;
		
		define_tag('add');
			if(self->event_data->find('googlecal_eventid') == '');
				local('tokens' = self->cal->g_auth->auth_tokens);
				//create google calendar entry
				local('event' = googlecal_event(map()));
				//#event->setStartEnd(self->event_data->find('evStartDate'), self->event_data->find('evEndDate'));
				#event->setFullDayEvent(self->event_data->find('evStartDate'), self->event_data->find('evEndDate'));
				#event->setSummary(self->event_data->find('evTitle')+' '+self->event_data->find('evCityState'));
				debug_log(' check event: '+#event->toJSON());
				
				local('event_result' = decode_json(self->cal->events_insert(self->getCalendarID, #event)));
				debug_log(' insert event: '+#event_result);
				
				//save data to local event record
				local('save_event' = 'update onsiteEvents set googlecal_id = "'+self->getCalendarID+'", googlecal_eventid = "'+#event_result->find('id')+'" where id = '+self->event_id+';');
				inline(-database="test", -table="onsiteEvents", -sql=#save_event);
				
				/inline;
				//update prefs
				local('tokens' = self->cal->g_auth->auth_tokens);
				local('googletokens' = ae_googletokens());
				
				#googletokens->updateAccess(#tokens->find('access_token'));
				#tokens->contains('refresh_token') ? #googletokens->updateRefresh(#tokens->find('refresh_token'));
			else;
				self->update;
			/if;
			
		/define_tag;
		
		define_tag('update');
			
			//update google calendar entry
			local('cal_list' = self->cal->calendarlist_get());
			
			local('event' = googlecal_event(map()));
			//#event->setStartEnd(self->event_data->find('evStartDate'), self->event_data->find('evEndDate'));
			#event->setFullDayEvent(self->event_data->find('evStartDate'), self->event_data->find('evEndDate'));
			#event->setSummary(self->event_data->find('evTitle')+' '+self->event_data->find('evCityState'));
			
			local('updatevalues' = #event->event_map);
			local('event_result' = decode_json(self->cal->events_update(self->getCalendarID, self->event_data->find('googlecal_eventid'), #updatevalues)));
			debug_log('googlesync update eventid:'+self->event_data->find('googlecal_eventid'));
			debug_log('googlesync update: '+#event_result);
			
		/define_tag;
		
		define_tag('remove');
			debug_log("using googlesync remove");
			
			//remove google calendar entry
			local('cal_list' = self->cal->calendarlist_get());
			
			debug_log("googlesync->remove eventid:"+self->event_data->find('googlecal_eventid'));
			local('event_result' = decode_json(self->cal->events_delete(self->getCalendarID, self->event_data->find('googlecal_eventid'))));
			debug_log("googlesync calendar delete result:"+#event_result);
			//remove data from local event record
			local('save_event' = 'update onsiteEvents set googlecal_id = null, googlecal_eventid = null where id = '+self->event_id+';');
			inline(-database="test", -table="onsiteEvents", -sql=#save_event);
				debug_log("googlesync->remove status:"+error_currenterror);
			/inline;
		/define_tag;
		
	/define_type;
	
	define_type('ae_googletokens');
		define_tag('onCreate');
		
		/define_tag;
		
		define_tag('fetch');
			local('findtokens' = 'select access_Token, refresh_Token, access_updated from googlecal_tokens where name = "default";');
			
			inline(-database="test", -sql=#findtokens);
				return(records_map(-type='array')->first->second);
			/inline;
				
		/define_tag;
		
		define_tag('updateAccess', -required='token');
			local('findtokens' = 'update googlecal_tokens set access_Token="'+#token+'" where name = "default";');
			
			inline(-database="test", -sql=#findtokens);
			
			/inline;
				
		/define_tag;
		
		define_tag('updateRefresh', -required='token');
			local('findtokens' = 'update googlecal_tokens set refresh_Token="'+#token+'" where name = "default";');
			
			inline(-database="test", -sql=#findtokens);
			
			/inline;
				
		/define_tag;
	/define_type;
?>