<?Lassoscript
	//custom lasso type to access Google Calendar API functions
	define_tag('br');
		return('<br/>');
	/define_tag;
	
	define_tag('debug_on');
		return(true);
	/define_tag;
	
	define_tag('debug_log', -required='logtext');
		debug_on ? log_critical('DEBUG:'+#logtext);
	/define_tag;
	
	define_tag('rest_request', -required='method', -required='url', -optional='rest_headers', -optional='request_body');
		// Must be loaded under os_process user permissions
		local('cmd' = 'curl');
		#cmd +=(' -X '+#method);
		if(local_defined('rest_headers'));
			local('header_string' = '');
			iterate(#rest_headers, local('header'));
				#header_string+=(#header->first+':'+#header->second+'\r\n');
			/iterate;
			#header_string->removetrailing('\r\n');
			
			#cmd +=(' -H "'+#header_string+'"');
			
		/if;
		if(local_defined('request_body'));
			#cmd +=(' -d \''+#request_body+'\'');
		/if;
		#cmd +=(' '+#url);
			local('curl_rest' = os_process(
		                '/bin/bash',
		                (: '-c', 'cd /;' + #cmd)));
		     local('response' = #curl_rest->read);
		     #curl_rest->close;
	     
	     return(#response);
	/define_tag;
	
	define_tag('googleApi_settings');
	
		local('api_settings' = map(
			'googlecal_api_url' = 'https://www.googleapis.com/calendar/v3/',
			'google_oauth_url' = 'https://accounts.google.com/o/oauth2/',
			'client_id' = 'ID from Google API account',
			'client_secret' = 'secret from Google API account',
			'redirect_uri' = 'must redirect to site page which will handle responses from google api',
			'auth_scope' = 'https://www.googleapis.com/auth/calendar'
			));
			//Auth Scope determines what kind of permissions you ask the user for.
		return(#api_settings);
		
	/define_tag;
	
	define_type('googlecal_api');
		
		local('googlecal_api_url' = googleApi_settings->find('googlecal_api_url'));
		
		local('g_auth' = google_auth());
		
		define_tag('onCreate');
			
		/define_tag;
		
		define_tag('acl_delete', -required='calendarID', -required='ruleID');
			//use REST Delete verb
			local('restoptions' = array(CURLOPT_CUSTOMREQUEST = 'DELETE'));
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID+'/acl/'+#ruleID));
			
			return(include_url(#req_url, -options = #restoptions));
			
		/define_tag;
		
		define_tag('acl_get', -required='calendarID', -required='ruleID');
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID+'/acl/'+#ruleID));
			
			return(include_url(#req_url));
		/define_tag;
		
		define_tag('acl_insert', -required='calendarID');
			
		/define_tag;
		
		define_tag('acl_list', -required='calendarID');
			
		/define_tag;
		
		define_tag('acl_update', -required='calendarID', -required='ruleID');
			
		/define_tag;
		
		define_tag('acl_patch', -required='calendarID', -required='ruleID');
			
		/define_tag;
		
		define_tag('calendarlist_delete', -required='calendarID');
			
		/define_tag;
		
		define_tag('calendarlist_get');
			debug_log('calendarlist_get');
			local('req_url' = (self->googlecal_api_url+'users/me/calendarList'));
			
			local('list_request' = include_url(#req_url, -sendMIMEheaders=self->g_auth->requestHeaders()));
			local('calendarlist' = googlecal_calendarList(#list_request));
			
			return(#calendarlist);
		/define_tag;
		
		define_tag('calendarlist_insert');
			
		/define_tag;
		
		define_tag('calendarlist_list');
			
		/define_tag;
		
		define_tag('calendarlist_update', -required='calendarID');
			
		/define_tag;
		
		define_tag('calendarlist_patch', -required='calendarID');
			
		/define_tag;
		
		define_tag('calendars_clear', -required='calendarID');
			
		/define_tag;
		
		define_tag('calendars_delete', -required='calendarID');
			
		/define_tag;
		
		define_tag('calendars_get', -required='calendarID');
			debug_log('calendars_get');
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID));
			local('calendar_response' = decode_json(include_url(#req_url, -sendMIMEheaders=self->g_auth->requestHeaders())));
			local('calendar' = googlecal_calendar(#calendar_response));
			
			return(#calendar);
		/define_tag;
		
		define_tag('calendars_insert');
			
		/define_tag;
		
		define_tag('calendars_update', -required='calendarID');
			
		/define_tag;
		
		define_tag('calendars_patch', -required='calendarID');
			
		/define_tag;
		
		define_tag('colors_get');
			
		/define_tag;
		
		define_tag('events_delete', -required='calendarID', -required='eventID');
			debug_log('events_delete');
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID+'/events/'+#eventid));
			local('event_response' = rest_request('DELETE', #req_url, self->g_auth->requestHeaders()));
			return(#event_response);
		/define_tag;
		
		define_tag('events_get', -required='calendarID', -required='eventID');
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID+'/events/'+#eventid));
			local('event_response' = (include_url(#req_url, -sendMIMEheaders=self->g_auth->requestHeaders())));
			local('event' = googlecal_event(#event_response));
			debug_log('events_get response: '+#event_response);
			return(#event);
		/define_tag;
		
		define_tag('events_import', -required='calendarID');
			
		/define_tag;
		
		define_tag('events_insert', -required='calendarID', -required='event');
			debug_log('events_insert');
			local('request_body' = #event->toJSON);
				
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID+'/events'));
			local('event_response' = (include_url(#req_url, -sendMIMEheaders=self->g_auth->requestHeaders(), -postparams=#request_body)));
			return(#event_response);
		/define_tag;
		
		define_tag('events_instances', -required='calendarID', -required='eventID');
			
		/define_tag;
		
		define_tag('events_list', -required='calendarID');
			debug_log('events_list');
			local('requeststring' = 'fields=');
			local('item_attr' = 'created,description,end,endTimeUnspecified,etag,iCalUID,id,kind,location,recurringEventId,sequence,start,status,summary,updated,visibility');
			local('requests' = 'items('+encode_url(#item_attr)+')');
			#requeststring+=#requests;
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID+'/events?'+#requeststring));
			local('event_response' = (include_url(#req_url, -sendMIMEheaders=self->g_auth->requestHeaders() )));
			
			return(#event_response);
		/define_tag;
		
		define_tag('events_move', -required='calendarID', -required='eventID');
			
		/define_tag;
		
		define_tag('events_quickadd', -required='calendarID');
			
		/define_tag;
		
		define_tag('events_update', -required='calendarID', -required='eventID', -required='updatevalues');
			debug_log('events_update');
			local('curr_event' = self->events_get(#calendarID, #eventID));
			
			debug_log('events_update currentevent: '+#curr_event->toJSON);
			iterate(#updatevalues, local('update'));
				#curr_event->event_map->insert(#update->first = #update->second);
			/iterate;
			
			local('request_body' = #curr_event->toJSON);
			
			debug_log('events_update request body: '+#request_body);
			local('req_url' = (self->googlecal_api_url+'calendars/'+#calendarID+'/events/'+#eventID));
			//local('update_response' = (include_url(#req_url, -sendMIMEheaders=self->g_auth->requestHeaders(), -postparams=#request_body, -options=array(CURLOPT_CUSTOMREQUEST='PUT'))));
			local('update_response' = rest_request('PUT', #req_url, self->g_auth->requestHeaders(), #request_body));
			
			debug_log('events_update response: '+#update_response);
			return(#update_response);
		/define_tag;
		
		define_tag('events_patch', -required='calendarID', -required='eventID');
			
		/define_tag;
		
		define_tag('freebusy_query');
			
		/define_tag;
		
		define_tag('settings_get', -required='setting');
			
		/define_tag;
		
		define_tag('settings_list');
			
		/define_tag;
		
		define_tag('makeRequest', -required='options', -required='url');
			return(include_url(#url, -options=#options));
		/define_tag;
		
	/define_type;
	
	define_type('googlecal_calendarList');
		
		local('etag');
		local('calendar_array' = array());
		local('cal_count');
		local('primaryid');
		
		define_tag('onCreate', -required='json_data');
			debug_log('calendarlist->onCreate');
			local('rawdata' = decode_json(#json_data));
			if(#rawdata->type == 'map' && #rawdata >> 'kind' && #rawdata->find('kind') == 'calendar#calendarList');
				self->etag = #rawdata->find('etag');
				local('cal_items' = #rawdata->find('items'));
				self->cal_count = #cal_items->size;
				iterate(#cal_items, local('cal_data'));
					if(#cal_data->type == 'map');
						local('cal_object' = googlecal_calendar(#cal_data));
						#cal_object->calPrimary ? self->primaryid = #cal_object->calID;
						
						self->calendar_array->insert(#cal_object);
					/if;
				/iterate;
			/*else;
				fail(-1, 'unknown CalendarList dataset type: '+#rawdata->type+', keys: '+#rawdata->keys+', kind present:'+(#rawdata >> 'kind')+', kind value: '+#rawdata->find('kind'))*/
			/if;
		/define_tag;
	/define_type;
	
	define_type('googlecal_calendar');
		local('etag');
		local('calID');
		local('calPrimary');
		
		local('cal_raw');
		
		define_tag('onCreate', -required='calendar_data');
			debug_log('calendar->onCreate');
			self->cal_raw = #calendar_data;
			
			self->etag = #calendar_data->find('etag');
			self->calID = #calendar_data->find('id');
			self->calPrimary = boolean(#calendar_data->find('primary'));
			debug_log('calendar id: '+self->calID);
		/define_tag;
		
	/define_type;
	
	define_type('googlecal_event');
		local('start');
		local('end');
		local('summary');
		local('created');
		
		local('raw_event');
		local('event_map' = map());
		local('date_format_str' = "%QT%T%z");
		local('fulldate_format_str' = "%Q");
		
		define_tag('onCreate', -required='event_data');
			debug_log('cal_event->onCreate '+(#event_data->type == 'bytes'));
			debug_log('eventdata type: '+#event_data->type);
			self->raw_event = #event_data;
			if(#event_data->type == 'bytes' || #event_data->type == 'string' );
				self->event_map = decode_json(#event_data);
			else;
				self->event_map = #event_data;
			/if;
			
			/*
				.event_map->forEachPair => {
					.set(#1)
				}
			*/
		/define_tag;
		
		define_tag('set', -required='valuepair');
			self->\(tag("'"+#valuepair->first+"'="))->invoke(#valuepair->second);
		/define_tag;
		
		define_tag('formatDates');
			self->start->setFormat('%Q%T%z');
			self->end->setFormat('%Q%T%z');
		/define_tag;
		
		define_tag('setStartEnd', -required='start', -required='end');
			debug_log('cal_event->setStartEnd');
			self->start = date(#start);
			self->end = date(#end);
			self->event_map->insert('start' = map('dateTime' = self->start->format(self->date_format_str)));
			self->event_map->insert('end' = map('dateTime' = self->end->format(self->date_format_str)));
			
		/define_tag;
		
		define_tag('setFullDayEvent', -required='start', -required='end');
			debug_log('cal_event_setFullDayEvent');
			self->start = date(#start);
			self->end = date(#end);
			self->start != self->end ? self->end->add(-day=1);
			self->event_map->insert('start' = map('date' = self->start->format(self->fulldate_format_str)));
			self->event_map->insert('end' = map('date' = self->end->format(self->fulldate_format_str)));
			
		/define_tag;
		
		define_tag('setSummary', -required='summary');
			debug_log('cal_event->setSummary');
			self->summary = #summary;
			self->event_map->insert('summary' = self->summary);
				
		/define_tag;
		
		define_tag('toMap');
			return(self->event_map);
			/*
			return map(
				'start' = map('dateTime' = .start->format(.date_format_str)),
				'end' = map('dateTime' = .end->format(.date_format_str)),
				'summary' = .summary
				)
			*/
		/define_tag;
		
		define_tag('debugToMap');
			return(map(
					'start' = self->start, 
					'end' = self->end,
					'summary' = self->summary,
					'raw' = self->raw_event
					));
		/define_tag;
		
		define_tag('toJSON');
			debug_log('cal_event->toJSON');
			self->raw_event->isA('string') ? return(self->raw_event) | return(encode_json(self->event_map));
			
		/define_tag;
		
	/define_type;
	
	define_type('google_auth');
		local('google_oauth_url' = googleApi_settings->find('google_oauth_url'));
		local('authcode' = '');
		local('client_id' = googleApi_settings->find('client_id'));
		local('client_secret' = googleApi_settings->find('client_secret'));
		local('redirect_uri' = googleApi_settings->find('redirect_uri'));
		local('auth_scope' = googleApi_settings->find('auth_scope'));
		
		local('refresh_token' = '');
		
		local('auth_tokens'=map());
		
		//local('oauth_client = lasso_oauth()
		
		define_tag('onCreate');
			//self->auth_tokens = ae_googletokens->fetch;
			//self->check_tokens();
			//return .auth_tokens
			debug_log('auth created');
		/define_tag;
		
		define_tag('getAuthCode');
			debug_log('getAuthCode');
			local('code_request_string' = self->google_oauth_url);
			local('approval_request' = (self->auth_tokens->contains('refresh_token') && self->auth_tokens->find('refresh_token') != '' ? 'auto' | 'force'));
			#code_request_string+='auth?response_type=code&client_id='+self->client_id+'&redirect_uri=' + encode_strictURL(self->redirect_uri) + '&scope=' + encode_strictURL(self->auth_scope) + '&access_type=offline&approval_prompt='+#approval_request;
			redirect_url(#code_request_string);
			
		/define_tag;
		
		define_tag('saveAuthCode', -required='authcode');
			self->authcode = #authcode;
			debug_log('saveAuthCode');
			return(self->requestAccessToken);
			
		/define_tag;
		
		define_tag('requestAccessToken');
			debug_log('requestAccessToken');
			local('request_params' = array(
				'code' = 			self->authcode,
				'client_id' = 		self->client_id,
				'client_secret' = 	self->client_secret,
				'redirect_uri' = 	self->redirect_uri,
				'grant_type' = 	'authorization_code'
				));
			local('AuthResponse' = include_url(self->google_oauth_url+'token', -postparams=#request_params));
			
			self->auth_tokens = decode_json(#authResponse);
			self->auth_tokens->insert('access_updated' = date(date));
			
			var('googleTokens' = self->auth_tokens);
			
			return(#authResponse);
		/define_tag;
		
		define_tag('refreshAccessToken', -optional='force');
			!local_defined('force') ? local('force' = false);
			
			debug_log('refreshAccessToken');
			self->auth_tokens->find('refresh_token') == '' ? self->getAuthCode;
			
			local('token_age' = duration(date(self->auth_tokens->find('access_updated')), date(date))->second);
			debug_log('age: '+#token_age);
			if(#token_age >= duration(self->auth_tokens->find('expires_in'))->second || #force);
				
				local('request_params' = array(
					'refresh_token' = 	self->auth_tokens->find('refresh_token'),
					'client_id' = 		self->client_id,
					'client_secret' = 	self->client_secret,
					'grant_type' = 	'refresh_token'
					));
				
				debug_log('params - '+#request_params);	
				local('AuthResponse' = include_url(self->google_oauth_url+'token', -postparams=#request_params));
				
				self->auth_tokens = decode_json(#authResponse);
				self->auth_tokens->insert('access_updated' = date(date));
				self->auth_tokens->insert(#request_params->find('refresh_token')->first);
				
				var('googleTokens' = self->auth_tokens);
			
			/if;
			//return .auth_tokens
		/define_tag;
		
		define_tag('check_tokens');
			local('debug_str' = '');
			debug_log('check_tokens');
			if(self->auth_tokens->size > 0);
				#debug_str->append(' refresh token ');
				self->refreshAccessToken();
			else;
				#debug_str->append(' get auth Code ');
				self->getAuthCode();
			/if;
			self->auth_tokens->insert('debug' = #debug_str);
			return(#debug_str);
		/define_tag;
		
		define_tag('revokeToken');
			include_url(self->google_oauth_url+'revoke?token='+self->auth_tokens->find('refresh_token'));
		/define_tag;
		
		define_tag('checkDuration');
			local('token_age' = duration(date(self->auth_tokens->find('access_updated')), date(date)));
			
			return('token age: '+(#token_age->second > duration(self->auth_tokens->find('expires_in'))->second)+'<br');
		/define_tag;
		
		define_tag('requestHeaders');
			return(array('Authorization' = 'Bearer '+self->auth_tokens->find('access_token'), 'Content-Type'='application/json'));
		/define_tag;
		
		
	/define_type;
?>